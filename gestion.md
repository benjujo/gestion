

# Identifique los principales aspectos de alineamiento TI – Estrategia de negocio.

Como el negocio de la FDA consiste en garantizar la seguridad y efectividad de los alimentos y drogas de la población, por lo que si apareciera algún componente nuevo debe existir la infraestructura tecnológica preparada para esto, el alineamento TI coresponde enfocarse en que su base de datos debiese ser lo suficientemente flexible como para integrar nuevos datos y categorías, además de garantizar la seguridad e integridad de su información debe alinearse con una fuerte política de ciberseguridad y un buen control de todas las situaciones que pueden llegar a ocurrir en el laboratiorio.
La estrategia de negocio, que corresponde cómo la organización puede lograr su misión, es **ofrecer soluciones seguras y de calidad** en beneficio de la salud pública. Para lograr esto será a través de la seguridad de sus plataformas otorguen confiabilidad a quienes las usen y la calidad de estas mismas de tal forma que la comunicación en estas sea fluida y efectiva. 


# Identifique aspectos de la estrategia del negocio que condicionan o pueden condicionar a futuro el desarrollo de las T.I.

El hecho que parte de su estrategia de negocio está también enfocada en la ciberseguridad, condiciona a que en un futuro siempre se esté permanentemente buscando posibles vulnerabilidades por lo que al momento de utilizar una nueva aplicación o alguna librería para desarrollar un producto nuevo será necesario testear este código con herramientas de pentesting para evitar posibles brechas de seguridad del código. Dentro de esta misma categoría está además el uso de implementar la autenticación de multi-factor, haciendo que toda aplicación utilizada en la organización deba tener esta cualidad y exigiéndo a sus empleados contar con dispositivos de seguridad como digipass, U2F, etc.
Otro aspecto relevante a destacar es el que planeen crear un framework estandarizado para el desarrollo de aplicaciones, ya que esto es el principal lineamiento al desarrollo de las aplicaciones futuras que estarán diseñadas a partir de dicho framework.


# Describa (contenido, no solo enumeración) los principales componentes de una arquitectura empresarial para FDA, en los aspectos en que sea posible. Incluya un mapa de stakeholders.

La arquitectura de negocio se divide en 5 partes:

1.  **Arquitectura de negocio**: En este caso están agrupados en tres pilares fundamentales:
    -   Seguridad y conformidad: Asegurar la seguridad, fiabilidad y precisión de los sistemas de la agencia según sea necesario y en apoyo de los reglamentos y mandatos clave.
    -   Calidad: Ofrecer productos y servicios de TI de alta calidad que son críticos para que la FDA cumpla con su misión y en apoyo de las necesidades administrativas y operativas relacionadas.
    -   Eficiencia: Proporcionar sistemas y servicios de TI de manera eficiente, efectiva y oportuna. Un ejemplo de esto sería identificar las aplicaciones en desuso para sacarlas o modernizarlas.
2.  **Arquitectura de información**: Ya que uno de los objetivos es reducir la redundancia de y aumentar la efectividad, la información debe mantenerse de la forma más reducida posible sin duplicaciones, por lo que el modelo de datos debe estar bastante bien normalizado. Además, debe tener réplicas en caso de pérdida de la información usando RAIDS
3.  **Arquitectura de aplicaciones**: Hay aplicaciones que deben estar orientadas al laboratorio (LIMS) usando información de los análisis que ahí se puedan efectuar, y estas aplicaciones deben estar aisladas de otros sistemas, para que los datos que aquí se generen no puedan perder integridad siendo manipulados por cualquier persona. En el caso de los administrativos, deben tener por ejemplo un sistema de remuneraciones conectado con un sistema de seguimiento de horas, siendo este sector el que mantenga la mayor interoperabilidad entre aplicaciones. La aplicación usada por los usuarios que sean de la población no debiese tener información crítica, ya que todo aqupi sería de acceso público. En este caso sería importante aisalar esta aplicación en una DMZ.
4.  **Arquitectura de conectividad**: Además de las aplicaciones que estarán aisladas por motivos de seguridad, aquellas aplicaciones que deben conversar entre sí deben ocupar protocolos de comunicación seguros, como mínimo SSL/TLS y HTTPS de tal manera que si existiese una vulnerabilidad donde se puedan interceptar las comunicaciones no se filtre información de los laboratorios o datos personales de las planillas administrativas. Entre los usuarios de la población general y resultados de laboratorios debiese existir un middleware que sea capaz de filtrar la información escencial e importante para la población sin mostrar más de lo necesario como operaciones confidenciales de laboratorio.
5.  **Arquitectura de infraestructura tecnológica**: La unidad de TI de la organización debe tener en su datacenter infraestructura que les permita réplica de datos, teniendo NAS en RAID. Deben haber terminales en todas las estaciones de trabajo de tal manera que todo el proceso se enceuntre digitalizado. Deben tener software para poder generar informes en cualquier punto del proceso.

![img](./mapa.png "Mapa de stakeholders")


# Consistente con lo anterior, genere un diagrama de arquitectura tecnológica a nivel general.

![img](./tecno.png "Diagrama de arquitectura tecnológica")

Como público general entra la población que esté interesada en la información.


# Consistente con lo anterior, proponga principales sistemas o aplicaciones propias del negocio (es decir, no las de soporte o administrativas) y su impacto en los objetivos estratégicos de FDA.

De los principales sistemas a consideran se encuentran las aplicaciones que controlan los procesos de los laboratorios (LIMS) y su principal aporte dentro de los objetivos estratégicos es generar de una forma **eficiente y optimizada** el análisis de las muestras de los distintos componentes que llegan para sus debidas pruebas.
Además para que el análisis de muestras y datos sea más expedita, es posible que se necesiten utilizar clusters para paralelizar distintos análisis de datos de esta forma también se lograría el objetivo de eficiencia.


# Las iniciativas tecnológicas no son solo sistemas computacionales. Proponga, consistentemente, principales iniciativas que no implican desarrollar o adquirir sistemas computacionales y que impactan en el logro de los objetivos estratégicos de FDA y en la ejecución de su estrategia T.I.

Un ejemplo de esto sería un buen manejo y providad al momento de usar credenciales de acceso a los distintos sistemas, es decir, que existan buenos hábitos en términos de seguridad como no compartir contraseñas entre empleados y cada uno utilice sus propias credenciales, que cada uno maneje un dispositivo de U2F para apalancar el riesgo de suplantación de identidad dentro de la organización teniendo esto un gran impacto positivo en términos de ciberseguridad.


# En forma coherente con lo anterior, enuncie principales consideraciones respecto de la capacidad (infraestructura) interna y su evolución en el tiempo, en particular para la integración de nuevas tecnologías de terceros.

Es necesario que dentro de la propia infraestructura de la organización cuente con un datacenter propio el cual maneje las aplicaciones usadas dentro de la organización como en los laboratorios, administración, etc. El datacenter no debe tener cracterísticas especiales que vayan más allá de su definición como reduncacia de proovedores de internet, redundancia del sistema eléctrico, etc.
En donde debe haber un mayor foco es respecto a la plataforma en la cual se ejecutarán las aplicaciones: El sistema debe ser escalable por lo que las aplicaciones deben permitir de forma sencilla una escalabilidad tanto horizontal como vertical siendo necesario el uso de contenedores teniendo la buena práctica de modularizar las aplicaciones y dejar cabida para nuevas aplicaciones.


# Identifique condiciones de mercado o regulatorias que pueden afectar el plan y explique.

Una condición de mercado es el hecho del modelo comercial de mover todo a la nube, pero el gran problema de esto es que se puede estar vulnerando la privacidad de los datos personales de las personas que ahí trabajan o de los productos analizados, de hecho, las regulaciones respecto a los datos personales y confidenciales dicen que no es posible almacenar esta información en territorios extranjeros, por lo que las aplicaciones y datos que estas generan deban permanecer en el datacenter propio de esta organización.


# Describa la mayor cantidad posible de espacios para el uso de estándares.

Las cosas que se peuden estandarizar en esta organización para así evitar desarrollar protocolos propios puede ser algún software de administración de personal y actividades diarias con algún ERP.
Para el caso de los laboratorios ya existen varios software LIMS que permiten tener un sistema de protocolos estándar dentro del laboratorio.
Otro elemento que debe ser estándar debe ser el uso de las APIs dentro de los aplicativos de la organización para que el uso de cada uno de estos sea de la forma más eficiente y modular posible sin necesidad de modificar varos componentes por un cambio.
La ciberseguridad de la organización también debe cumplir ciertos estándares como lo detalla en plan, seguir ciertos protocolos de seguridad realizados por NIST.


# Reconozca requerimientos regulatorios (tales como privacidad, seguridad, protección de los derechos del cliente, etc.) del caso.

Los datos generados en los laboratorios deben ser confidenciales, tales como quién realizó el análisis además de quién pidió que realizara para evitar posibles conflictos de inteŕes dentro de la organización, es por esto, que el sistema de laboratorios debería contar con un mecanismo de protección de la privacidad en ambos sentidos de la comunicaión.
Otro requerimiento regulatorio es el uso al cual serán sometidos estos datos no sean de interés comercial para evitar posibles conflictos de interés.


# Identifique y argumente principales componentes de costos (naturalmente no se pide que se estimen costos).

Los costos principales de este plan es la infraestructura computacional que se debe tener. Primeramente se identifican los terminales en los que trabajn todos los empleados de la organización que deben estar asegurados para que sean utilizados exclusivamente por los empleados.
La red interna debe ser lo suficientemente rápida como para manjear una comunicación efectiva entre todos los integrantes de la organización, pero el uso de fibra óptica estaría sobrevalorado. Bastaría con conectar todo el lugar con cable ethernet y router por cada división.
El datacenter de la organización se lleva la gran parte de los costos, ya que en este se consideran los racks, los servidores, discos, NAS, citas para las copias de seguridad, UPS, grupos electrógenos, etc.
Además se debe contar con una planilla de soporte técnicos disponibles en caso de algúna falla o desconocimiento del uso de algun elemento de este plan.


# Reconozca espacios de incremento de eficiencia operacional en el negocio por la aplicación de este plan.

Principalmente se verían beneficiados los procesos de laboratorio y administrativo ya que al mejorar las comunicaciones entre todos los participantes de la organización, las labores en estos sectores se realizarían de forma más eficiente.


# Reconozca espacios de participación de terceros, y profundidad de dicha participación, incluyendo las ventajas y desventajas contra la alternativa de privilegiar la capacidad interna.

Un tercero en este caso sería contratar infraestructura como servicio para hostear ahí las aplicaciones. Este tercero tendría una gran relevancia en el plan, ya que de este dependería la disponibilidad del servicio. Una de las ventajas es justamente esa, que la disponibilidad estaría garantizada por un tercero pero la gran desventaja es que no se tienen control de las aplicaciones que están corriendo ni mucho menos de los datos que estas generan, por lo que puede ser una brecha de privacidad muy grande.


# Coherentemente con lo anterior, reconozca roles externalizables y no externalizables, argumentando.

Un rol externalizable puede ser la base de datos pública de todos los componentes y los informes que se presentan a la población. Esta información es pública y además debe estar siempre disponible por lo que si sería útil tenerlo en algún serivicio comercial de nube. En el caso de las plataformas que se usen en los laboratorios o las planillas administrativas estas no debiesen ser externalizables debido a que se estaría vulnerando la privacidad y confidencialidad de la organización.


# Caracterice mecanismos y responsables organizacionales para la priorización de proyectos y recursos.

Cada aplicación que se desarrollará o esté actualmente en uso debe tener a un responsable a cargo. Para cada proceso se debe dividir en etapas de prefactibilidad y factibilidad haciendo informes para evaluar si un proyecto puede llegar a ser viable o no. Pueden realizarse árboles de decisión para analizar cada proyecto y flujos de cajas para evaluar sus costos y beneficios.


# Dadas las características del negocio, identifique condiciones o niveles mínimos de disponibilidad del soporte tecnológico, y recuperación desde desastres.

Dado que la utilización de software es crítica ya que ofrece una aplicación a la población que debe estar siempre en línea, es necesario que esta tenga un uptime sobre el 99%. En el caso de la disponibilidad de los servicios administrativos basta con que estén arriba durante las horas de trabajo, por lo que es posible en horas de la noche realizar copias de seguridad en frío pero aún así es muy importante mantener la disponibilidad de la información de estos servicios si estarán conectados con otras aplicaciones. Los servicios deben contar con copias de seguridad periódicas de tal forma que en caso de cualquier pérdida de información y estas copias de seguridad deben ser almacenadas en lugares ajenos a las instalaciones de tal forma de dispersar la posibilidad de pérdida de información. Los discos que se usen para lamacenar la información de los aplicativos deben estar en RAID para otorgarle redundancia a la información y  estar preparados en caso de falla de algún disco. Debe tener más de una salida de un proovedor de internet, ya que debe ser capaz de seguir funcionando en caso de alguna caída de la red por parte de un proovedor de internet.


# Caracterice las principales capacidades requeridas de soporte interno -dada la estrategia de ejecución del plan- y de proveedores externos de T.I. para proveer y operar el plan definido. Ponga foco en aquellas que son singulares dado el negocio.

Dado que externalizar datos críticos de los laboratorios puede consitutir un problema de confidencialidad de la información  o información personal puede ir en contra de la regulación de datos personales, estas aplicaciones no deben estar en un servicio de nube externalizado, por lo que debe ser parte del soporte interno. En el caso de las aplicaciones que están expuetas para el resto de la población, es posible manejarlas fuera del soporte interno pudiendo externalizarse ya que la información que manejan son datos públicos como la seguridad de un alimento en cuestión y además deben estar en todo momento en línea, po lo que existen alternativas que estén siempre en línea.

