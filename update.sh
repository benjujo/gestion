#!/bin/bash
emacsclient --eval "(let ((file \"$(pwd)/gestion.org\"))
    (with-current-buffer (find-file-noselect file)
    (progn
        (org-latex-export-to-pdf)
	(let ((org-export-with-drawers t))
        (org-md-export-to-markdown)))))"
cp gestion.md README.md
git add gestion.md gestion.org gestion.tex gestion.pdf README.md 
git commit -m "Automatic push"
git push
